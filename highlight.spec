Name:           highlight
Summary:        Universal source code to formatted text converter
Version:        4.1
Release:        1
Group:          Development/Tools
License:        GPLv3

URL:            http://www.andre-simon.de/
Source0:        http://www.andre-simon.de/zip/%{name}-%{version}.tar.bz2

BuildRequires:  qt5-qtbase-devel
BuildRequires:  lua-devel, boost-devel
BuildRequires:  desktop-file-utils
BuildRequires:  gcc-c++ make

%{?filter_setup:
%filter_from_provides /^perl(/d;
%filter_from_requires /^perl(/d;
%filter_from_requires /^\/bin\/lua/d;
%filter_setup
}

%description
A utility that converts sourcecode to HTML, XHTML, RTF, LaTeX, TeX,
XSL-FO, XML or ANSI escape sequences with syntax highlighting.
It supports several programming and markup languages.
Language descriptions are configurable and support regular expressions.
The utility offers indentation and reformatting capabilities.
It is easily possible to create new language definitions and colour themes.

%package        gui
Summary:        GUI for the hihghlight source code formatter
Requires:       %{name} = %{version}-%{release}

%description    gui
A Qt-based GUI for the highlight source code formatter source.

%prep
%autosetup

%build
CFLAGS="$CFLAGS -fPIC %{optflags}"; export CFLAGS
CXXFLAGS="$CXXFLAGS -fPIC %{optflags}"; export CXXFLAGS
LDFLAGS="$LDFLAGS %{?__global_ldflags}"; export LDFLAGS

# disabled paralell builds to fix FTBFS on rawhide & highlight 3.52+
#make_build all gui           CFLAGS="${CFLAGS}"          \
 %{__make} all gui            CFLAGS="${CFLAGS}"          \
                              CXXFLAGS="${CXXFLAGS}"      \
                              LDFLAGS="${LDFLAGS}"        \
                              LFLAGS="-Wl,-O1 ${LDFLAGS}" \
                              PREFIX="%{_prefix}"         \
                              conf_dir="%{_sysconfdir}/highlight/" \
                              QMAKE="%{_qt5_qmake}"       \
                              QMAKE_STRIP=

%install
%make_install PREFIX="%{_prefix}" conf_dir="%{_sysconfdir}/highlight/"

mkdir -p $RPM_BUILD_ROOT%{_datadir}/applications
mkdir -p $RPM_BUILD_ROOT%{_datadir}/pixmaps
make install-gui DESTDIR=$RPM_BUILD_ROOT PREFIX="%{_prefix}" conf_dir="%{_sysconfdir}/highlight/"

rm -rf $RPM_BUILD_ROOT%{_docdir}/%{name}/

desktop-file-install \
    --dir $RPM_BUILD_ROOT%{_datadir}/applications \
   highlight.desktop

%files
%{_bindir}/highlight
%{_datadir}/highlight/
%{_mandir}/man1/highlight.1*
%{_mandir}/man5/filetypes.conf.5*
%config(noreplace) %{_sysconfdir}/highlight/

%doc ChangeLog* AUTHORS README* extras/
%license COPYING

%files gui
%{_bindir}/highlight-gui
%{_datadir}/applications/highlight.desktop
%{_datadir}/icons/hicolor/256x256/apps/highlight.png

%changelog
* Thu Jul 28 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 4.1-1
- Update to 4.1

* Feb Aug 17 2021 Python_Bot <Python_Bot@openeuler.org> - 3.42-1
- Init Package
